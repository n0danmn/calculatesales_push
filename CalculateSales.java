package jp.alhinc.iwahori_arisa.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CalculateSales {
	public static void main(String[] args){
//		System.out.println("ここにあるファイルを開きます => " + args[0]);
		
		Map <String, String> branchNameMap = new HashMap <String, String>();
		Map <String, Long> totalMap = new HashMap <>();
		
				
		BufferedReader br = null;
		try {
			File file = new File(args[0],"branch.lst");
			if(!file.exists()){
				System.out.println("支店名義ファイルが存在しません");	
				
			}
			FileReader fr = new FileReader(file);
			br = new BufferedReader(fr);
			
			String line;
			
			//branch.lstの内容を一行ずつだしますよ
			while((line = br.readLine()) != null) {
//				System.out.println(line);
				
				//取り出した一行を","を境目に分割しますよ　splitを使って
				String[] items = line.split(",");
				
				//System.out.println(items[1]);
				//System.out.println(items.length);
				int how = items.length;
				if(!(how == 2)){
					System.out.println("支店定義ファイルのフォーマットが不正です");
					return;
				}
				
				//分割した内容のうちitems[0]に振り分けられた支店コードは数字三桁だけになってますか
				if(!items[0].matches ("^[0-9]{3}$")){
					System.out.println("支店定義ファイルのフォーマットが不正です");
					return;
									}
				//分割したものをそれぞれマップに振り分けて、値を出す						
				branchNameMap.put(items[0],items[1]);
				totalMap.put(items[0],0L);
//				System.out.println(branchNameMap.get(items[0]));
			}
		} catch(IOException e) {
			System.out.println("エラーが発生しました。");
		} finally {
			if(br != null) {
				try{
					br.close();
				} catch(IOException e) {
					System.out.println("closeできませんでした。");
				}
			}
		}
		
		File dir = new File(args[0]);	 
		File[] lists = dir.listFiles();
		ArrayList<File> BigFile = new ArrayList<>(); 
		List<Long> NumberFile = new ArrayList<>();
				
		//Listsと名付けたファイルの中のもので行う繰り返し動作
		for(int i =0; i < lists.length; i++){
//		System.out.println(lists[i]);
			
			//繰り返し行う動作の中で、("")の条件に合うものを取り出す
			if(lists[i].getName().matches("\\d{8}.rcd")){
//			 	System.out.println(lists[i].getName());
			 	String codeName = (lists[i].getName());
//			 	System.out.println(codeName.substring(0,8));
			 	long onlyNum = Long.parseLong (codeName.substring(0,8));
//			 	System.out.println(onlyNum);
			 	NumberFile.add(onlyNum);
			 				 	
				//取り出したものをBigFileに溜め込む	BigFileのひとつずつの型はFile
				BigFile.add (lists[i]) ;
			}
		}
//		System.out.println(NumberFile);
		for(int i=1; i < NumberFile.size(); i++){			
			if(NumberFile.get(i) - NumberFile.get(i-1) != 1){
				System.out.println("売上ファイル名が連番になっていません");
				return;
			}		
		}
		
		//溜め込んだものを表示する		 
//		 System.out.println(BigFile);
		
		//BigFile内のデータ分繰り返す
		for(int i =0; i < BigFile.size(); i++){
			try {
				//
//				System.out.println(BigFile.get(i));
				FileReader fr = new FileReader(BigFile.get(i));
				br = new BufferedReader(fr);
				
				String line;
				
				//BigFileの各ファイルごとの内容を一行ずつだす、出した内容をまとめる箱をrcdBoxとする					
				ArrayList<String> rcdBox = new ArrayList<>();
				
				while((line = br.readLine()) != null) {
//					System.out.println(line);
					
					//rcdBoxに加えていく、１ファイル終わったらwhileループ終了
					rcdBox.add(line);
				}
				System.out.println(rcdBox);
				
				//totalMapにキーに支店コード、値に売上額を入れた
				Long before = totalMap.get(rcdBox.get(0));
				Long after = before + Long.parseLong(rcdBox.get(1));
				totalMap.put(rcdBox.get(0), after);
										
				
			} catch(IOException e) {
				System.out.println("エラーが発生しました。");
			} finally {
				
				if(br != null) {
					try{
						br.close();
					} catch(IOException e) {
						System.out.println("closeできませんでした。");
					}
				}
			}
		}
		
		//これまでの内容を外部ファイルへ出力
		BufferedWriter bw =null;
		try{
			File file1 = new File("C:\\Users\\iwahori.arisa\\CalculateSales\\branch.out");
			FileWriter fw = new FileWriter(file1);
			bw = new BufferedWriter(fw);
			for(Map.Entry<String, String> entry : branchNameMap.entrySet()){
//				System.out.println(entry.getKey());
//			    System.out.println(entry.getValue());
//			    System.out.println(totalMap.get(entry.getKey()));
			    bw.write(entry.getKey() +","+ entry.getValue() +","+ totalMap.get(entry.getKey()));
			    bw.newLine();
			}
//				bw.write(rcdBox.get(0),items[1], after);
		} catch(IOException e) {
			System.out.println("エラーが発生しました。");
		} finally {
			
			if(bw != null) {
				try{
					bw.close();
				} catch(IOException e) {
					System.out.println("closeできませんでした。");
				}
			}
		}

//		System.out.println(totalMap);
	
	} 

}